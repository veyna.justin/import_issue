# Import Issue

A repository to demonstrate an issue I am having with imports. If you know why this issue is happening and how to fix it please explain.

## Description

When importing from a separate sub-module of the same library I run into a "module not found" error.
I work arount the problem by importing the module with more specific syntax. ie:
```python
from imis import foo  # Fails

from imis.a.code_a import foo  # Works

from ..a import foo  # Works
```